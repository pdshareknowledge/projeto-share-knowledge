/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senac.rn;

import com.senac.bd.EventooBD;

import com.senac.bean.Eventoo;
import com.senac.bean.Usuario;

import java.util.List;

import javax.jws.WebService;

/**
 *
 * @author Mateus
 */
@WebService
public class EventooRN implements com.senac.util.CrudGenerico<Eventoo> {

    private EventooBD eventooBD;

    public EventooRN() {
        eventooBD = new EventooBD();
    }

    @Override
    public void salvar(Eventoo eventoo) {

        if (eventoo.getTituloEvento() == null || "".equals(eventoo.getTituloEvento())) {
            throw new RuntimeException("Campo Título deve ser Obrigatório!");
        }
        if (eventoo.getDescricaoEvento()== null || "".equals(eventoo.getDescricaoEvento())) {
            throw new RuntimeException("Campo Descrição deve ser Obrigatório!");
        }
        if (eventoo.getTituloStream() == null || "".equals(eventoo.getTituloStream())) {
            throw new RuntimeException("Campo Passwords deve ser Obrigatório!");
        }
        if (eventoo.getDescricaoStream() == null || "".equals(eventoo.getDescricaoStream())) {
            throw new RuntimeException("Campo Passwords deve ser Obrigatório!");
        }
        if (eventoo.getDataInicio() == null || "".equals(eventoo.getDataInicio())) {
            throw new RuntimeException("Campo Passwords deve ser Obrigatório!");
        }
        if (eventoo.getDataTermino() == null || "".equals(eventoo.getDataTermino())) {
            throw new RuntimeException("Campo Passwords deve ser Obrigatório!");
        }
        eventooBD.salvar(eventoo);
    }

    @Override
    public void excluir(Eventoo eventoo) {
        eventooBD.excluir(eventoo);
    }

    @Override
    public List<Eventoo> listar(Eventoo eventoo) {
        return null;
    }

    public Usuario buscar(Eventoo eventoo) {
        return null;
    }

    public List<Eventoo> pesquisa(String tituloEvento) {
        return eventooBD.pesquisaEvento(tituloEvento);
    }
    
}
