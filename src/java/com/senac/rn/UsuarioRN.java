/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senac.rn;

import com.senac.bd.UsuarioBD;
import com.senac.bean.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Mateus
 */
@WebService
public class UsuarioRN implements com.senac.util.CrudGenerico<Usuario>{

    private UsuarioBD usuarioBD;
    
    public UsuarioRN() {
        usuarioBD = new UsuarioBD();
    }
    
    @Override
    public void salvar(Usuario usuarioBean) {
        
        if(usuarioBean.getNome() == null || "".equals(usuarioBean.getNome())){
            throw new RuntimeException("Campo Login deve ser Obrigatório!");
        }
        if (usuarioBean.getSobrenome()== null || "".equals(usuarioBean.getSobrenome())) {
            throw new RuntimeException("Campo Passwords deve ser Obrigatório!");
        }
        if (usuarioBean.getEmail()== null || "".equals(usuarioBean.getEmail())) {
            throw new RuntimeException("Campo Passwords deve ser Obrigatório!");
        }
        if (usuarioBean.getSenha()== null || "".equals(usuarioBean.getSenha())) {
            throw new RuntimeException("Campo Passwords deve ser Obrigatório!");
        }
        usuarioBD.salvar(usuarioBean);
    }

    @Override
    public void excluir(Usuario usuario) {
        usuarioBD.excluir(usuario);
    }

    @Override
    public List<Usuario> listar(Usuario usuario) {
        
        return usuarioBD.listar(usuario);
    }
    
    public Usuario buscar(Usuario bean) {
        return usuarioBD.consultar(bean);
    }
    
    public List<Usuario> find(String email, String senha) {
        return usuarioBD.findUsuario(email, senha);
    }

    public List<Usuario> pesquisa(String nome){        
        return usuarioBD.pesquisaUsuario(nome);
    }
}
