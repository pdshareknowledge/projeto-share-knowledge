/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senac.rn;

import com.senac.bd.CategoriaBD;
import com.senac.bean.Categoria;
import java.util.List;
import javax.jws.WebService;

@WebService
public class CategoriaRN implements com.senac.util.CrudGenerico<Categoria>{
    
    private  CategoriaBD categoriaBD;

    public CategoriaRN() {
        categoriaBD = new CategoriaBD();
    }
    
    @Override
    public void salvar(Categoria categoriaBean) {
        
        if(categoriaBean.getNome() == null || "".equals(categoriaBean.getNome())){
            throw new RuntimeException("Campo Nome deve ser Obrigatório!");
        }
        categoriaBD.salvar(categoriaBean);
    }

    @Override
    public void excluir(Categoria categoria) {
        categoriaBD.excluir(categoria);
    }

    @Override
    public List<Categoria> listar(Categoria categoria) {
        return categoriaBD.listar(categoria);
    }

    public List<Categoria> pesquisa(String nome) {

        return categoriaBD.pesquisaCategoria(nome);
    }

    public Object pesquisarPorId(Integer codigo) {
        return this.categoriaBD.pesquisarPorId(codigo);
    }
}
