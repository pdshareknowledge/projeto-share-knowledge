CREATE DATABASE "ShareknowledgePU"
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Portuguese_Brazil.1252'
       LC_CTYPE = 'Portuguese_Brazil.1252'
       CONNECTION LIMIT = -1;


 
CREATE TABLE STREAM (
  id_stream SERIAL   NOT NULL ,
  titulo VARCHAR(50)   NOT NULL ,
  data_inicio VARCHAR(50)   NOT NULL ,
  data_termino VARCHAR(50)    ,
  data_criacao VARCHAR(50)   NOT NULL ,
  url VARCHAR(50)   NOT NULL   ,
  chave VARCHAR(100)   NOT NULL   ,
PRIMARY KEY(id_stream));




CREATE TABLE USUARIO (
  id_usuario SERIAL   NOT NULL ,
  nome VARCHAR(50) NOT NULL,
  sobrenome VARCHAR(50) NOT NULL,
  email VARCHAR(50)   NOT NULL ,
  senha VARCHAR(50)   NOT NULL   ,
PRIMARY KEY(id_usuario));




CREATE TABLE CATEGORIA (
  id_categoria SERIAL   NOT NULL ,
  nome VARCHAR(50)   NOT NULL   ,
  tag VARCHAR(100),
PRIMARY KEY(id_categoria));




CREATE TABLE EVENTO (
  id_evento SERIAL   NOT NULL ,
  id_stream INTEGER   NOT NULL ,
  id_categoria INTEGER   NOT NULL ,
  id_usuario INTEGER   NOT NULL ,
  titulo VARCHAR(50)   NOT NULL ,
  tipo VARCHAR(50)   NOT NULL   ,
  curtida INTEGER,
  
PRIMARY KEY(id_evento),
  FOREIGN KEY(id_usuario)
    REFERENCES USUARIO(id_usuario),
  FOREIGN KEY(id_categoria)
    REFERENCES CATEGORIA(id_categoria),
  FOREIGN KEY(id_stream)
    REFERENCES STREAM(id_stream));


CREATE INDEX IFK_Rel_01 ON EVENTO (id_usuario);
CREATE INDEX IFK_Rel_02 ON EVENTO (id_categoria);
CREATE INDEX IFK_Rel_03 ON EVENTO (id_stream);


CREATE TABLE INSCRICAO (
  id_inscricao SERIAL NOT NULL,
  id_evento INTEGER  NOT NULL,
  id_usuario INTEGER  NOT NULL,
  inscricoes INTEGER,
  PRIMARY KEY(id_inscricao),
  FOREIGN KEY(id_evento)
    REFERENCES EVENTO(id_evento),
  FOREIGN KEY(id_usuario)
    REFERENCES USUARIO(id_usuario)
);

