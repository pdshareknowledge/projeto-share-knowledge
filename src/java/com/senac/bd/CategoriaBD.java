/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senac.bd;

import com.senac.bean.Categoria;
import com.senac.rn.CategoriaRN;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Mateus
 */
public class CategoriaBD implements com.senac.util.CrudGenerico<Categoria>{
    private EntityManager em;
    Categoria categoria = new Categoria();

    public CategoriaBD() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ShareknowledgePU");
        em = emf.createEntityManager();
    }
    
    @Override
    public void salvar(Categoria categoriaBean) {
        em.getTransaction().begin();
        em.merge(categoriaBean);
        em.getTransaction().commit();
    }

    @Override
    public void excluir(Categoria categoria) {
        em.getTransaction().begin();
        em.remove(em.find(Categoria.class, categoria.getIdCategoria()));
        em.getTransaction().commit();
    }

    @Override
    public List<Categoria> listar(Categoria categoria) {
        StringBuilder sb = new StringBuilder("SELECT c FROM Categoria c");

        if (categoria.getIdCategoria()!= null && categoria.getIdCategoria() != 0) {
            sb.append("and c.idCategoria=:i");
        }
        if (categoria.getNome() != null) {
            sb.append("and c.nome linke :n");
        }
        if (categoria.getTag()!= null) {
            sb.append("and c.sobrenome linke :t");
        }

        Query qry = em.createQuery(sb.toString());
        if (categoria.getIdCategoria() != null && categoria.getIdCategoria() != 0) {
            qry.setParameter("i", categoria.getIdCategoria());
        }
        if (categoria.getNome() != null) {
            qry.setParameter("n", categoria.getNome());
        }
        if (categoria.getTag() != null) {
            qry.setParameter("t", categoria.getTag());
        }
        return qry.getResultList();
    }
    
    public List<Categoria> pesquisaCategoria(String nome) {
        Query query = em.createQuery("Select c FROM Categoria c where c.nome = :nome", Categoria.class);
        query.setParameter("nome", nome);

        return query.getResultList();

    }

    public Integer pesquisarPorId(Integer value) {
        
        
        return categoria.getIdCategoria();
    }
}
