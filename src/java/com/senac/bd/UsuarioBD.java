/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senac.bd;

import com.senac.bean.Usuario;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Mateus
 */
public class UsuarioBD implements com.senac.util.CrudGenerico<Usuario>{
    private EntityManager em;

    public UsuarioBD() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ShareknowledgePU");
        em = emf.createEntityManager();
    }
    
    @Override
    public void salvar(Usuario usuario) {
        em.getTransaction().begin();
        em.merge(usuario);
        em.getTransaction().commit();
    }

    @Override
    public void excluir(Usuario usuario) {
        em.getTransaction().begin();
        em.remove(em.find(Usuario.class, usuario.getIdUsuario()));
        em.getTransaction().commit();
    }

    @Override
    public List<Usuario> listar(Usuario usuario) {
        StringBuilder sb = new StringBuilder("SELECT u FROM Usuario u where 1=1 ");
        
        if(usuario.getIdUsuario() != null && usuario.getIdUsuario() !=0){
            sb.append("and u.idUsuario=:i");
        }
        if(usuario.getNome() != null){
            sb.append("and u.nome like :n");
        }
        if(usuario.getSobrenome() != null){
            sb.append("and u.sobrenome like :s");
        }
        if(usuario.getEmail() != null){
            sb.append("and u.email like :e");
        }
        if(usuario.getSenha() != null){
            sb.append("and u.senha like :p");
        }
        
        Query qry = em.createQuery(sb.toString());
        if (usuario.getIdUsuario() != null && usuario.getIdUsuario() != 0) {
            qry.setParameter("i", usuario.getIdUsuario());
        }
        if (usuario.getNome() != null) {
            qry.setParameter("n", "%" + usuario.getNome() + "%");
        }
        if (usuario.getSobrenome() != null) {
            qry.setParameter("s", "%" + usuario.getSobrenome()+ "%");
        }
        if (usuario.getEmail() != null) {
            qry.setParameter("e", "%" + usuario.getEmail()+ "%");
        }
        if (usuario.getSenha() != null) {
            qry.setParameter("p", "%" + usuario.getSenha()+ "%");
        }
        return qry.getResultList();
    }
    
    public Usuario consultar(Usuario bean){
        return em.find(Usuario.class, bean.getEmail());
    }
    
    public List<Usuario> findUsuario(String email, String senha) {
        Query query = em.createQuery("Select u FROM Usuario u where u.email = :email and u.senha = :senha" , Usuario.class);
        query.setParameter("email", email);
        query.setParameter("senha", senha);
        return query.getResultList();
    }
    
    public List<Usuario> pesquisaUsuario(String nome){
        Query query = em.createQuery("Select u FROM Usuario u where u.nome = :nome", Usuario.class);
        query.setParameter("nome", nome);
        return query.getResultList();
        
    }

    private EntityManager getEntityManager() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
    
