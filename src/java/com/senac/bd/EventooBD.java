/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senac.bd;

import com.senac.bean.Eventoo;
import com.senac.bean.Usuario;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Mateus
 */
public class EventooBD implements com.senac.util.CrudGenerico<Eventoo>{
    private EntityManager em;
    
    public EventooBD() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ShareknowledgePU");
        em = emf.createEntityManager();
    }

    
    @Override
    public void salvar(Eventoo eventoo) {
        em.getTransaction().begin();
        em.merge(eventoo);
        em.getTransaction().commit();
    }

    @Override
    public void excluir(Eventoo eventoo) {
        em.getTransaction().begin();
        em.remove(em.find(Eventoo.class, eventoo.getIdEvento()));
        em.getTransaction().commit();
    }


    public Usuario consultar(Eventoo eventoo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Eventoo> listar(Eventoo bean) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    public List<Eventoo> pesquisaEvento(String tituloEvento) {
        Query query = em.createQuery("Select u FROM Eventoo u where u.titulo_evento = :titulo_evento", Eventoo.class);
        query.setParameter("titulo_evento", tituloEvento);
        return query.getResultList();
    }
    
    
}
