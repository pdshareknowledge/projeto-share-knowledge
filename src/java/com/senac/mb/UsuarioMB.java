/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senac.mb;

import com.senac.bean.Usuario;
import com.senac.rn.UsuarioRN;
import com.senac.util.Mensagem;
import com.sun.xml.ws.rx.testing.PacketFiltering;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


/**
 *
 * @author Mateus
 */

@ManagedBean

public class UsuarioMB {

    private Usuario usuario;
    private UsuarioRN usuarioRN;
    private List<Usuario> usuarios;
    private String pesquisa;
    
    public UsuarioMB() {
        usuario = new Usuario();
        usuarioRN = new UsuarioRN();
        usuarios = usuarioRN.listar(usuario);     
    }
    
    public void limparTudo(){
        pesquisa = "";
    }

    public void editar(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public String pesquisar(){
        try{
            usuarios = usuarioRN.pesquisa(pesquisa);
            Mensagem.add("Operação executada com sucesso!");
            return "listarUsuarios";
        } catch(Exception e){
            Mensagem.error(e.getMessage());
        }
        return null;   
    }
    public List<Usuario> lista() {
        try{
            return usuarios;
        } catch(Exception e){
            Mensagem.error(e.getMessage());
        }
        return null;
    }

    public String salvar() {
        try{
            usuarioRN.salvar(usuario);
            Mensagem.add("Operação executada com sucesso!");
            usuario = new Usuario();
            usuarios = usuarioRN.listar(usuario);
            return "listarUsuarios";
        }catch(Exception e){
            Mensagem.error(e.getMessage());
        }
        return null;
    }

    public void limpar() {
        usuario = new Usuario();
    }
    
    public String excluir(Usuario u) {
        try {
            usuarioRN.excluir(u);
            Mensagem.add("Operação executada com sucesso!");
            usuario = new Usuario();
            usuarios = usuarioRN.listar(usuario);
            return "listarUsuarios";
        } catch (Exception e) {
            Mensagem.error(e.getMessage());
        }
        return null;    
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }


    
    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    /**
     * @return the pesquisa
     */
    public String getPesquisa() {
        return pesquisa;
    }

    /**
     * @param pesquisa the pesquisa to set
     */
    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }
    
    
    
}
