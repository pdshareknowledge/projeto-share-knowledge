/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senac.mb;

import com.google.api.services.samples.youtube.cmdline.live.ListBroadcasts;
import com.google.api.services.youtube.model.LiveBroadcast;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author MacLeo
 */
@ManagedBean
@RequestScoped
public class ListarStreaming {
  /*
  - Id: x6Fl7to3Ndo
  - Title: Transmissão ao vivo de Leonardo Faria
  - Description: 
  - Published At: 2015-09-17T23:07:38.000Z
  - Scheduled Start Time: 1970-01-01T00:00:00.000Z
  - Scheduled End Time: null
   */
    
    public List<LiveBroadcast> listaEventos;
    
    /**
     * 
     */
    public ListarStreaming(){
        
    }
    
    public List<LiveBroadcast> listarVideos(){
        return ListBroadcasts.listBroadcast();
    }
    
    /**
     * @return the listaEventos
     */
    public List<LiveBroadcast> getListaEventos() {
        return listaEventos;
    }

    /**
     * @param listaEventos the listaEventos to set
     */
    public void setListaEventos(List<LiveBroadcast> listaEventos) {
        this.listaEventos = listaEventos;
    }
}
