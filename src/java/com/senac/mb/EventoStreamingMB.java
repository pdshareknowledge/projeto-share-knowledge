/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senac.mb;

import com.google.api.services.samples.youtube.cmdline.live.CreateBroadcast;
import com.senac.bean.Eventoo;
import com.senac.rn.EventooRN;
import com.senac.util.Mensagem;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author MacLeo
 */
@ManagedBean
public class EventoStreamingMB {
    
    private Date minDate;
    private Date minDateEnd;
    private Eventoo eventoo;
    private EventooRN eventooRN;
    private String pesquisa;
    private List<Eventoo> eventoos;
    
    public EventoStreamingMB() {
        limparTudo();
    }
    
    public void limparTudo(){
        this.minDate = new Date();
        eventoo = new Eventoo();
        eventooRN = new EventooRN();       
    }
    
    public String salvar(){
        
        try {
            CreateBroadcast.createStreaming(eventoo.getTituloEvento(), eventoo.getDescricaoEvento(), 
                    eventoo.getTituloStream(), eventoo.getDescricaoStream(),
                    eventoo.getDataInicio(), eventoo.getDataTermino());
            eventooRN.salvar(eventoo);
            Mensagem.add("Operação executada com sucesso!");
            eventoo = new Eventoo();
            return "listarEventos";
        } catch (Exception e) {
            Mensagem.error(e.getMessage());
        }
       return null; 
    }
    
    public String pesquisar() {
        try {
            eventooRN.pesquisa(pesquisa);
            Mensagem.add("Operação executada com sucesso!");
            return "listarEventos";
        } catch (Exception e) {
            Mensagem.error(e.getMessage());
        }
        return null;
    }

    public Date setMinEndDate(Date date){
        minDateEnd = minDate;
        minDateEnd.setMinutes(date.getMinutes()+10);
        return minDateEnd;
    }

    /**
     * @return the minDate
     */
    public Date getMinDate() {
        return minDate;
    }

    /**
     * @param minDate the minDate to set
     */
    public void setMinDate(Date minDate) {
        this.minDate = minDate;
    }

    /**
     * @return the minDateEnd
     */
    public Date getMinDateEnd() {
        return minDateEnd;
    }

    /**
     * @param minDateEnd the minDateEnd to set
     */
    public void setMinDateEnd(Date minDateEnd) {
        this.minDateEnd = minDateEnd;
    }

    public Eventoo getEventoo() {
        return eventoo;
    }

    public void setEventoo(Eventoo eventoo) {
        this.eventoo = eventoo;
    }

    public EventooRN getEventooRN() {
        return eventooRN;
    }

    public void setEventooRN(EventooRN eventooRN) {
        this.eventooRN = eventooRN;
    }

    /**
     * @return the pesquisa
     */
    public String getPesquisa() {
        return pesquisa;
    }

    /**
     * @param pesquisa the pesquisa to set
     */
    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    /**
     * @return the eventoos
     */
    public List<Eventoo> getEventoos() {
        return eventoos;
    }

    /**
     * @param eventoos the eventoos to set
     */
    public void setEventoos(List<Eventoo> eventoos) {
        this.eventoos = eventoos;
    }
    
    
}
