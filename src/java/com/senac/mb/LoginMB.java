package com.senac.mb;

import com.senac.bean.Usuario;
import com.senac.rn.UsuarioRN;
import com.senac.util.Mensagem;
import com.senac.util.SessionUtil;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class LoginMB {

    private String email;
    private String senha;
       
    public LoginMB() {
    }
/*
    public String autenticar(){
        if("admin@gmail.com".equals(email) && "admin".equals(senha)){
            return "listarEventos";
        }else{
            return "Dados inválidos";
        }
    }
    
    System.out.println("autentica..");

     if ("admin@gmail.com".equals(email) && "admin".equals(senha)) {
     System.out.println("Confirmou  usuario e senha ...");

     //ADD USUARIO NA SESSION
     Object b = new Object();

     SessionUtil.setParam("USUARIOLogado", b);

     return "listarEventos.xhtml?faces-redirect=true";

     } else {

     return null;

     }
  */  
    public String autentica() {
       try{         
           UsuarioRN usuarioRN = new UsuarioRN();
           List<Usuario> usuario = usuarioRN.find(email, senha);
        if(!usuario.get(0).equals(null)){
            return "listarEventos";
        }
       }catch(Exception e){
            Mensagem.error("Dados inválidos!");
        }
           return "index";
       }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }
}
    
    

