/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senac.mb;

import com.senac.bean.Categoria;
import com.senac.rn.CategoriaRN;
import com.senac.util.Mensagem;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Mateus
 */
@ManagedBean
public class CategoriaMB {
    
    private Categoria categoria;
    private CategoriaRN categoriaRN;
    private List<Categoria> categorias;
    private String pesquisa;

    public CategoriaMB(){
        categoria = new Categoria();
        categoriaRN = new CategoriaRN();
        categorias = categoriaRN.listar(categoria);
        pesquisa = "";    
    }
    
    public void limparTudo() {
        pesquisa = "";
    }
    
    public void editar(Categoria categoria){
           this.categoria = categoria;
    }

    public CategoriaRN getCategoriaRN() {
        return categoriaRN;
    }

    public void setCategoriaRN(CategoriaRN categoriaRN) {
        this.categoriaRN = categoriaRN;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }
    
    
    
    public String pesquisar() {
        try {
            categorias = categoriaRN.pesquisa(pesquisa);
            Mensagem.add("Operação executada com sucesso!");
            return "listarCategorias";
        } catch (Exception e) {
            Mensagem.error(e.getMessage());
        }
        return null;

    }

    public void limpar() {
        categoria = new Categoria();
    }

    public List<Categoria> lista() {
        try {
            return categorias;
        } catch (Exception e) {
            Mensagem.error(e.getMessage());
        }
        return null;
    }

    public String salvar() {
        try{
            categoriaRN.salvar(categoria);
            Mensagem.add("Operação executada com sucesso!");
            categoria = new Categoria();
            categorias = categoriaRN.listar(categoria);
            
            return "listarCategorias";
        }catch(Exception e){
            Mensagem.error(e.getMessage());
        }
        return null;
    }

    public String excluir(Categoria categoria) {
        try{
            categoriaRN.excluir(categoria);
            Mensagem.add("Operação executada com sucesso!");
            categoria = new Categoria();
            categorias = categoriaRN.listar(categoria);
            return "listarCategorias";
        }catch (Exception e){
            Mensagem.error(e.getMessage());
        }
        return null;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public List<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }

    
    
    
}
