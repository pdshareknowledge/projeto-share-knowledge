/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senac.bean;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Mateus
 */
@Entity
@Table(name = "stream")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Stream.findAll", query = "SELECT s FROM Stream s"),
    @NamedQuery(name = "Stream.findByIdStream", query = "SELECT s FROM Stream s WHERE s.idStream = :idStream"),
    @NamedQuery(name = "Stream.findByTitulo", query = "SELECT s FROM Stream s WHERE s.titulo = :titulo"),
    @NamedQuery(name = "Stream.findByDataInicio", query = "SELECT s FROM Stream s WHERE s.dataInicio = :dataInicio"),
    @NamedQuery(name = "Stream.findByDataTermino", query = "SELECT s FROM Stream s WHERE s.dataTermino = :dataTermino"),
    @NamedQuery(name = "Stream.findByDataCriacao", query = "SELECT s FROM Stream s WHERE s.dataCriacao = :dataCriacao"),
    @NamedQuery(name = "Stream.findByUrl", query = "SELECT s FROM Stream s WHERE s.url = :url"),
    @NamedQuery(name = "Stream.findByChave", query = "SELECT s FROM Stream s WHERE s.chave = :chave")})
public class Stream implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_stream")
    private Integer idStream;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "titulo")
    private String titulo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "data_inicio")
    private String dataInicio;
    @Size(max = 50)
    @Column(name = "data_termino")
    private String dataTermino;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "data_criacao")
    private String dataCriacao;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "url")
    private String url;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "chave")
    private String chave;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idStream")
    private List<Evento> eventoList;

    public Stream() {
    }

    public Stream(Integer idStream) {
        this.idStream = idStream;
    }

    public Stream(Integer idStream, String titulo, String dataInicio, String dataCriacao, String url, String chave) {
        this.idStream = idStream;
        this.titulo = titulo;
        this.dataInicio = dataInicio;
        this.dataCriacao = dataCriacao;
        this.url = url;
        this.chave = chave;
    }

    public Integer getIdStream() {
        return idStream;
    }

    public void setIdStream(Integer idStream) {
        this.idStream = idStream;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataTermino() {
        return dataTermino;
    }

    public void setDataTermino(String dataTermino) {
        this.dataTermino = dataTermino;
    }

    public String getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(String dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getChave() {
        return chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }

    @XmlTransient
    public List<Evento> getEventoList() {
        return eventoList;
    }

    public void setEventoList(List<Evento> eventoList) {
        this.eventoList = eventoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStream != null ? idStream.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Stream)) {
            return false;
        }
        Stream other = (Stream) object;
        if ((this.idStream == null && other.idStream != null) || (this.idStream != null && !this.idStream.equals(other.idStream))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.senac.bean.Stream[ idStream=" + idStream + " ]";
    }
    
}
