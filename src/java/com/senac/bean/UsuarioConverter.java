package com.senac.bean;
import com.senac.mb.UsuarioMB;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;





@FacesConverter(value = "UsuarioConverter")
public class UsuarioConverter implements Converter {

    /**
     * Método executado no momento de transformar
     * a String que representa o objeto na tela
     * em forma de objeto novamente para atribuir
     * no managed bean.
     * @param context
     * @param component
     * @param value
     * @return 
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        /*
            Supondo que aqui aconteça o acesso as RNs do sistema (SELECT)
            pesquisando com base no ID o objeto completo a ser convertido.
        */
        List<Usuario> listaUsuarios = new UsuarioMB().lista();
        for(Usuario u : listaUsuarios) {
            if(u.getIdUsuario().toString().equals(value)) {
                return u;
            }
        }
        throw new RuntimeException("Erro geral!!!");
    }

    /**
     * Método executado no momento de escrever
     * o objeto na tela, em forma de String.
     * @param context
     * @param component
     * @param value
     * @return 
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        /*
            Supondo que aqui aconteça o acesso as RNs do sistema (SELECT)
            pesquisando com base no ID o objeto completo a ser convertido.
        */
        List<Usuario> listaUsuarios = new UsuarioMB().lista();
        for(Usuario u : listaUsuarios) {
            if(u.equals((Usuario) value)) {
                return u.getIdUsuario().toString();
            }
        }
        throw new RuntimeException("Erro geral!!!");
    }
    
}
