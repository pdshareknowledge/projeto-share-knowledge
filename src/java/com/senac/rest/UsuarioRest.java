/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.senac.rest;

import com.senac.bean.Usuario;
import com.senac.rn.UsuarioRN;
import com.senac.util.Mensagem;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Mateus
 */
@Path("/usuarios")
public class UsuarioRest {
    
    private Usuario usuario;
    private UsuarioRN usuarioRN;
    private List<Usuario> usuarios;
    private String pesquisa;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/listar")
    public Response lista(Usuario usuario) {
        try {
            List<Usuario> usuarios = usuarioRN.listar(null);
            
            return gerarResponseParaCollection(usuarios);
            
        } catch (Exception e) {
            Mensagem.error(e.getMessage());
        }
        return null;
    }
    
    public Response gerarResponseParaCollection(List<Usuario> obj) {
        if (obj == null || obj.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        GenericEntity<List<Usuario>> lista = new GenericEntity<List<Usuario>>(obj) {
        };
        return Response.ok(lista).build();
    }
}
